angular.module('TokenModule', [])

  .controller('GeneratedTokenListCtrl', ['$scope', function ($scope) {
    $scope.apps = _.map(window.tokens, function (app) {
      app.media          = app.media || null;
      app.appName        = app.appName || null;
      app.consumerKey    = app.consumerKey || null;
      app.consumerSecret = app.consumerSecret || null;
      return app;
    });

    $scope.tokens = [];
    _.each($scope.apps, function (app) {
      var tokens = app.accessTokens;
      _.each(tokens, function (token) {
        token.appId             = app.id;
        token.id                = token.id || null;
        token.accessToken       = token.accessToken || null;
        token.accessTokenSecret = token.accessTokenSecret || null;
        token.expiresIn         = token.expiresIn || null;
        token.userId            = token.userId || null;
        token.screenName        = token.screenName || null;
        $scope.tokens.push(token);
      });
    });

    $('#app-list').dataTable({
      data   : $scope.apps,
      columns: [
        {title: 'ID', data: 'id'},
        {title: 'Media', data: 'media'},
        {title: 'App. Name', data: 'appName'},
        {title: 'Consumer Key', data: 'consumerKey'},
        {title: 'Consumer Secret', data: 'consumerSecret'}
      ]
    });

    $('#token-list').dataTable({
      data   : $scope.tokens,
      columns: [
        {title: 'ID', data: 'id'},
        {title: 'App ID', data: 'appId'},
        {title: 'Access Token', data: 'accessToken'},
        {title: 'Access Token Secret', data: 'accessTokenSecret'},
        {title: 'Expires In', data: 'expiresIn'},
        {title: 'User ID', data: 'userId'},
        {title: 'Screen Name', data: 'screenName'}
      ]
    });
  }]);