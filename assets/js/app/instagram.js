/**
 * Created by AndiNugroho on 20/04/2015.
 */
angular.module('InstagramModule', [])
  .service('InstagramRestApi', ['$http',
    function($http) {
      return function(endpoint, param) {
        return $http({
          url: '/instagram/rest_api',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            endpoint: endpoint,
            param: param
          }
        });
      }
    }
  ])
  .controller('UserToIdConverter', ['$scope', 'InstagramRestApi',
    function($scope, InstagramRestApi) {
      $scope.userName = null;
      $scope.result = null;
      $scope.convert = function() {
        $scope.result = null;
        if (!$scope.UserToIdConverter.$valid) return;
        var uname = $scope.userName + "";
        var endpoint = "users/search/";
        InstagramRestApi(endpoint, {
          q: uname.toLowerCase()
        })
          .success(function(data) {
            var found = false;
            var users = data.data;
            var i = 0;
            $scope.rawResult = "0000000000";
            while (!found && i < users.length) {
              if (users[i].username.toLowerCase() == uname.toLowerCase() || users[i].full_name.toLowerCase() == uname.toLowerCase()) {
                found = true
                $scope.rawResult = users[i].id;
              }
              i++;
            }
            $scope.result = angular.toJson(data, 2);
          })
          .error(function(err) {
            $scope.rawResult = "0000000000";
            $scope.result = angular.toJson(err, 2);
          });
      };
    }
  ]);