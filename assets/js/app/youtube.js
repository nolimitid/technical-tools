/**
 * Created by AndiNugroho on 20/04/2015.
 */
angular.module('YoutubeModule', [])
  .controller('UserToChannelConverter', ['$scope', '$http', function ($scope, $http) {
    $scope.userId  = null;
    $scope.result  = null;
    $scope.convert = function () {
      $scope.result = null;
      if (!$scope.userToChannelConverter.$valid) return;

      $http
        .get('https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername=' + $scope.userId + '&key=AIzaSyDyPhO_Evynwdh2AwHBcVl7jfr7GfAh3xo')
        .success(function (data) {
          $scope.rawResult = data;
          $scope.result    = angular.toJson(data, 2);
        })
        .error(function (err) {
          $scope.rawResult = data;
          $scope.result    = angular.toJson(err, 2);
        });
    };
  }]);