/**
 * Created by AndiNugroho on 20/04/2015.
 */
angular.module('LinkedinModule', [])
  .service('LinkedinRestApi', ['$http',
    function($http) {
      return function(endpoint, param) {
        return $http({
          url: '/linkedin/rest_api',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            endpoint: endpoint,
            param: param
          }
        });
      }
    }
  ])
  .controller('UserToIdConverter', ['$scope', 'LinkedinRestApi',
    function($scope, LinkedinRestApi) {
      $scope.userName = null;
      $scope.result = null;
      $scope.convert = function() {
        $scope.result = null;
        if (!$scope.UserToIdConverter.$valid) return;
        var uname = $scope.userName + "";
        var endpoint = "company-search/";
        LinkedinRestApi(endpoint, {
          keywords: uname.toLowerCase()
        })
          .success(function(data) {
            var found = false;
            var companies = data.companies.values;
            var i = 0;
            $scope.rawResult = "0000000000";
            while (!found && i < companies.length) {
              if (companies[i].name.toLowerCase() == uname.toLowerCase()) {
                found = true
                $scope.rawResult = companies[i].id;
              }
              i++;
            }
            $scope.result = angular.toJson(data, 2);
          })
          .error(function(err) {
            $scope.rawResult = "0000000000";
            $scope.result = angular.toJson(err, 2);
          });
      };
    }
  ]);