angular.module('TwitterModule', [])

.service('TwitterRestApi', ['$http',
  function($http) {
    return function(endpoint, param) {
      return $http({
        url: '/twitter/rest_api',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          endpoint: endpoint
        }
      });
    }
  }
])

.controller('UsersLookup', ['$scope', 'TwitterRestApi',
  function($scope, TwitterRestApi) {
    $scope.ids = null;
    $scope.result = null;
    $scope.get = function() {
      $scope.result = null;
      if (!$scope.form.$valid) return;

      TwitterRestApi('users/lookup', 'get', {
        screen_name: $scope.ids
      })
        .success(function(data) {
          $scope.rawResult = data;
          $scope.result = angular.toJson(data, 2);
        })
        .error(function(err) {
          $scope.rawResult = err;
          $scope.result = angular.toJson(err, 2);
        });
    };
  }
])

.controller('StatusesLookup', ['$scope', 'TwitterRestApi',
  function($scope, TwitterRestApi) {
    $scope.ids = null;
    $scope.result = null;
    $scope.get = function() {
      $scope.result = null;
      if (!$scope.form.$valid) return;

      TwitterRestApi('statuses/lookup', 'get', {
        id: $scope.ids
      })
        .success(function(data) {
          $scope.rawResult = data;
          $scope.result = angular.toJson(data, 2);
        })
        .error(function(err) {
          $scope.rawResult = err;
          $scope.result = angular.toJson(err, 2);
        });
    };
  }
]);