angular.module('ObjectModule', [])

  .value('BaseApi', {
    url: 'http://base.platform.nolimitid.com'
  })

  .service('ObjectService', ['$http', 'BaseApi', function ($http, BaseApi) {
    return {
      add   : function (data) {
        return $http({
          url    : BaseApi.url + '/api/object/add',
          method : 'POST',
          headers: {'Content-Type': 'application/json'},
          data   : data
        });
      },
      remove: function (data) {
        return $http({
          url    : BaseApi.url + '/api/object/remove',
          method : 'POST',
          headers: {'Content-Type': 'application/json'},
          data   : data
        });
      }
    }
  }])

  .service('IsDataTable', [function () {
    return function (nTable) {
      var settings = $.fn.dataTableSettings;
      for (var i = 0, iLen = settings.length; i < iLen; i++) {
        if (settings[i].nTable == nTable) {
          return true;
        }
      }
      return false;
    }
  }])

  .controller('ObjectAdd', ['$scope', 'ObjectService', function ($scope, ObjectService) {
    $scope.streamTypes = [
      {id: 'account', name: 'Account'},
      {id: 'keyword', name: 'Keyword'}
    ];
    $scope.medias      = [
      {id: 'twitter', name: 'Twitter'},
      {id: 'facebook', name: 'Facebook'},
      {id: 'youtube', name: 'YouTube'},
      {id: 'instagram', name: 'Instagram'},
      {id: 'askxl', name: 'Ask XL'},
      {id: 'rss', name: 'RSS'}
    ];
    $scope.selected    = {
      stream_type: $scope.streamTypes[0],
      media_id   : $scope.medias[0]
    };
    $scope.data        = {
      app_id       : null,
      app_secret   : null,
      object_name  : null,
      media_id     : $scope.selected.media_id.id,
      stream_type  : $scope.selected.stream_type.id,
      content      : null,
      extra_content: null
    };
    $scope.$watch('selected', function (newVal) {
      _.each(newVal, function (data, key) {
        $scope.data[key] = data.id;
      });
    }, true);

    $scope.submit = function () {
      if (!$scope.form.$valid) return;

      ObjectService
        .add($scope.data)
        .success(function (data) {
          new PNotify({
            title: 'Add/ Update Object Success',
            text : angular.toJson(data.object, 2),
            type : 'success',
            hide : false
          });
        })
        .error(function (err) {
          new PNotify({
            title: 'Add/ Update Object Failed',
            text : err,
            type : 'error',
            hide : false
          });
        });
    };
  }])

  .controller('ObjectRemove', ['$scope', 'ObjectService', function ($scope, ObjectService) {
    $scope.streamTypes = [
      {id: 'account', name: 'Account'},
      {id: 'keyword', name: 'Keyword'}
    ];
    $scope.medias      = [
      {id: 'twitter', name: 'Twitter'},
      {id: 'facebook', name: 'Facebook'},
      {id: 'youtube', name: 'YouTube'},
      {id: 'instagram', name: 'Instagram'},
      {id: 'askxl', name: 'Ask XL'},
      {id: 'rss', name: 'RSS'}
    ];
    $scope.selected    = {
      stream_type: $scope.streamTypes[0],
      media_id   : $scope.medias[0]
    };
    $scope.data        = {
      app_id     : null,
      app_secret : null,
      media_id   : $scope.selected.media_id.id,
      stream_type: $scope.selected.stream_type.id,
      content    : null
    };
    $scope.$watch('selected', function (newVal) {
      _.each(newVal, function (data, key) {
        $scope.data[key] = data.id;
      });
    }, true);

    $scope.submit = function () {
      if (!$scope.form.$valid) return;

      ObjectService
        .remove($scope.data)
        .success(function (data) {
          new PNotify({
            title: 'Remove Object Success',
            text : angular.toJson(data.object, 2),
            type : 'success',
            hide : false
          });
        })
        .error(function (err) {
          new PNotify({
            title: 'Remove Object Failed',
            text : err,
            type : 'error',
            hide : false
          });
        });
    };
  }])

  .controller('ObjectList', ['$scope', '$http', function ($scope, $http) {
    $scope.objects = null;
    var table      = null;

    $scope.$watch('objects', function (newVal) {
      if (newVal) {
        if (table) table.destroy();
        table = $('#object-list').DataTable({
          data   : $scope.objects,
          columns: [
            {title: 'Media Name', data: 'media'},
            {title: 'Name', data: 'object_name'},
            {title: 'Stream Type', data: 'stream_type'},
            {title: 'Content', data: 'content'},
            {title: 'Extra Content', data: 'extra_content'},
            {title: 'Priority', data: 'priority'}
          ]
        });
      }
    }, true);

    $http
      .get('http://base.platform.nolimitid.com/api/object/all')
      .then(function (objects) {
        $scope.objects = objects.data;
        return;
      })
      .catch(function (err) {
        window.alert(err);
      });
  }])

  .controller('ObjectAppList', ['$scope', '$http', 'IsDataTable', function ($scope, $http, IsDataTable) {
    $scope.objects   = null;
    $scope.appId     = null;
    $scope.appSecret = null;
    var table        = null;

    $scope.$watch('objects', function (newVal) {
      if (newVal) {
        var elem = $('#object-app-list');
        if (table) table.destroy();
        table = elem.DataTable({
          data   : $scope.objects,
          columns: [
            {title: 'Media Name', data: 'media'},
            {title: 'Name', data: 'object_name'},
            {title: 'Stream Type', data: 'stream_type'},
            {title: 'Content', data: 'content'},
            {title: 'Extra Content', data: 'extra_content'},
            {title: 'Priority', data: 'priority'}
          ]
        });
      }
    }, true);

    $scope.submit = function () {
      if (!$scope.form.$valid) return;

      $http
        .get('http://base.platform.nolimitid.com/api/object/app?app_id=' + $scope.appId + '&app_secret=' + $scope.appSecret)
        .success(function (objects) {
          $scope.objects = objects;
        })
        .error(function (err) {
          window.alert(err);
        });
    };
  }]);