//"use strict";
//var user = {
//  updateBasicProfile: function () {
//    var serial = $('form#user-basic-profile').serializeObject();
//    $
//      .ajax('/user/' + serial.id, {
//        method  : 'PUT',
//        data    : serial,
//        dataType: 'json'
//      })
//      .success(function (res) {
//        window.location.replace('/');
//      })
//      .error(function (err) {
//        alert(err);
//      });
//  }
//};

angular.module('UserModule', [])

  .controller('UserProfileCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.data   = {
      userId  : window.user.id,
      username: window.user.username,
      email   : window.user.email,
      password: null
    };
    $scope.submit = function () {
      if (!$scope.form.$valid) return;

      $http({
        url    : '/user/' + $scope.data.userId,
        method : 'PUT',
        headers: {'Content-Type': 'application/json'},
        data   : _.omit($scope.data, 'userId', 'password')
      })
        .then(function () {
          if ($scope.data.password) {
            return $http({
              url   : '/user/create_password',
              method: 'POST',
              data  : {
                password: $scope.data.password
              }
            });
          } else {
            return;
          }
        })
        .then(function () {
          window.location.replace('/');
        })
        .catch(function (err) {
          window.alert(JSON.stringify(err));
        });
    }
  }]);