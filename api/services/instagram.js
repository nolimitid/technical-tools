var qs = require('querystring');
var request = require('request');

var instagram = {
  access_token_key: "218878771.467ede5.10c7ef787a75424aae884c97e4a9cf2e",
  baseAPI: "https://api.instagram.com/v1/",
  restApi: function(endpoint, param) {
    var that = this;
    return new Promise(function(resolve, reject) {
      var url = that.baseAPI + endpoint + "?access_token=" + that.access_token_key + "&" + qs.stringify(param);
      request.get(url, function(err, response, body) {
        if (err) return reject(err);
        resolve(body);
      });
    });
  }
};

module.exports = instagram;