var qs = require('querystring');
var request = require('request');

var linkedin = {
  access_token_key: "AQXkdnj9B_zuXkwwUfDZEfEkSNqhbCRw0cfDgmx0mFvXgPcNNqq1XDV2pTwu610ltXCqtQMAcAIbVgbVon9wm5Vi4KJZzs7TpRUWKS2W9WM4szyrRVyzpxAMUCVSswndC8k0QcMQOUUmpvaHDPRXF42yYDXHk9mrZ20jKqSmBVisQfLXP20",
  baseAPI: "https://api.linkedin.com/v1/",
  restApi: function(endpoint, param) {
    var that = this;
    return new Promise(function(resolve, reject) {
      var url = that.baseAPI + endpoint + "?format=json&oauth2_access_token=" + that.access_token_key + "&" + qs.stringify(param);
      request.get(url, function(err, response, body) {
        if (err) return reject(err);
        resolve(body);
      });
    });
  }
};

module.exports = linkedin;