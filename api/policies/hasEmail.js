/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function (req, res, next) {
  User.findOne({id: req.user.id}).exec(function (err, user) {
    if (err) return res.negotiate(err);

    if (!user.email) return res.redirect('/user/profile');

    next();
  });
};
