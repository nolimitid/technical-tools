/**
 * AppController
 *
 * @description :: Server-side logic for managing apps
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  main: function(req, res) {
    res.view('homepage');
  },
  generate_token: function(req, res) {
    res.view('token/request');
  },
  generated_tokens: function(req, res) {
    Token.find().exec(function(err, tokens) {
      if (err) return res.negotiate(err);

      res.view('token/list', {
        title: 'Generated Tokens',
        tokens: tokens
      });
    });
  },
  youtube: function(req, res) {
    res.view();
  },
  twitter: function(req, res) {
    res.view();
  },
  object: function(req, res) {
    res.view();
  },
  instagram: function(req, res) {
    res.view();
  },
  linkedin: function(req, res) {
    res.view();
  }
};