/**
 * ApiController
 *
 * @description :: Server-side logic for managing apis
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var request = require('request');

module.exports = {
  tokens: function (req, res) {
    Token.find().exec(function (err, tokens) {
      if (err) return res.negotiate(err);

      res.json(tokens);
    });
  }
};
