/**
 * InstagramController
 *
 * @description :: Server-side logic for managing instagram
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  rest_api: function(req, res) {
    var endpoint = req.param('endpoint') || null,
      param = req.param('param') || null;

    if (!(endpoint && param)) return res.badRequest('please specify \'endpoint\' and \'param\' parameters')

    var paramObject = null;
    if (typeof param === 'object') {
      paramObject = param;
    } else {
      try {
        paramObject = JSON.parse(param);
      } catch (e) {
        return res.badRequest('can\'t parse \'param\' parameter into object');
      }
    }

    instagram
      .restApi(endpoint, paramObject)
      .then(function(result) {
        res.send(result);
      })
      .catch(function(err) {
        res.negotiate(err);
      });
  }
};