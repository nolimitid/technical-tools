/**
 * TokenController
 *
 * @description :: Server-side logic for managing tokens
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var request     = require('request'),
    twitterAPI  = require('node-twitter-api-edit'),
    querystring = require('querystring');

module.exports = {
  facebook_hooks : function (req, res) {
    var code      = req.param('code') || null,
        state     = req.param('state') || null,
        error     = req.param('error') || null,
        errorDesc = req.param('error_description') || null;

    if (error) return res.negotiate(new Error(errorDesc));

    Token
      .findOne({
        state: state
      })
      .exec(function (err, token) {
        if (err) return res.negotiate(err);

        if (!token) return res.serverError(new Error('there\'s an error in database'));

        var data = {
          code         : code,
          redirect_uri : token.redirectUri,
          client_id    : token.consumerKey,
          client_secret: token.consumerSecret
        };

        var url = format('https://graph.facebook.com/v2.3/oauth/access_token');

        request.post({url: url, form: data}, function (error, response, body) {
          if (error) {
            console.error(error);
            return res.negotiate(error);
          }

          var parsed = null;
          try {
            parsed = JSON.parse(body);
          } catch (e) {
            return res.negotiate(e);
          }

          var accessToken = parsed.access_token,
              expiresIn   = parsed.expires_in,
              tokenType   = parsed.token_type;

          if (typeof token.accessTokens === 'undefined') token.accessTokens = [];

          var exist = _.some(token.accessTokens, {accessToken: accessToken});

          if (!exist) {
            token.accessTokens.push({
              accessToken: accessToken,
              expiresIn  : expiresIn,
              tokenType  : tokenType
            });
          }

          token.save(function (err, saved) {
            if (err) return res.negotiate(err);

            saved.accessTokens = saved.accessTokens.reverse();

            res.view('token/list', {
              title : 'Facebook Generated Token',
              tokens: [saved]
            });
          });

        });
      });
  },
  facebook       : function (req, res) {
    var data           = {
          response_type: 'code',
          client_id    : req.param('consumer_key'),
          redirect_uri : format('%s/token/facebook_hooks', sails.getBaseurl()),
          state        : (Math.floor(Math.random() * Math.pow(10, 20))).toString()
        },
        consumerSecret = req.param('consumer_secret'),
        appName        = req.param('app_name') || null;

    if (!(req.param('consumer_key') && req.param('consumer_secret'))) return res.badRequest('please give consumer_key, consumer_secret');

    var url = format('https://www.facebook.com/dialog/oauth?%s', querystring.stringify(data));

    Token.findOrCreate(
      {
        consumerKey   : data.client_id,
        consumerSecret: consumerSecret,
        media         : 'facebook'
      },
      {
        consumerKey   : data.client_id,
        consumerSecret: consumerSecret,
        media         : 'facebook'
      },
      function (err, token) {
        if (err) return res.negotiate(err);

        token.state       = data.state;
        token.redirectUri = data.redirect_uri;
        token.appName     = token.appName || null;
        token.userId      = req.user.id;

        if (appName) token.appName = appName;

        token.save(function (err, saved) {
          if (err) return res.negotiate(err);

          res.redirect(url);
        });
      });
  },
  instagram_hooks: function (req, res) {
    var code      = req.param('code') || null,
        state     = req.param('state') || null,
        error     = req.param('error') || null,
        errorDesc = req.param('error_description') || null;

    if (error) return res.negotiate(new Error(errorDesc));

    Token
      .findOne({
        state: state
      })
      .exec(function (err, token) {
        if (err) return res.negotiate(err);

        if (!token) return res.serverError(new Error('there\'s an error in database'));

        var data = {
          grant_type   : 'authorization_code',
          code         : code,
          redirect_uri : token.redirectUri,
          client_id    : token.consumerKey,
          client_secret: token.consumerSecret
        };

        var url = format('https://api.instagram.com/oauth/access_token');

        request.post({url: url, form: data}, function (error, response, body) {
          if (error) {
            console.error(error);
            return res.negotiate(error);
          }

          var parsed = null;
          try {
            parsed = JSON.parse(body);
          } catch (e) {
            return res.negotiate(e);
          }

          var accessToken = parsed.access_token,
              userId      = parsed.user.id,
              screenName  = parsed.user.username,
              fullName    = parsed.user.full_name,
              userPicture = parsed.user.profile_picture;

          if (typeof token.accessTokens === 'undefined') token.accessTokens = [];

          var exist = _.some(token.accessTokens, {accessToken: accessToken});

          if (!exist) {
            token.accessTokens.push({
              accessToken: accessToken,
              userId     : userId,
              screenName : screenName,
              fullName   : fullName,
              userPicture: userPicture
            });
          }

          token.save(function (err, saved) {
            if (err) return res.negotiate(err);

            saved.accessTokens = saved.accessTokens.reverse();

            res.view('token/list', {
              title : 'Instagram Generated Token',
              tokens: [saved]
            });
          });

        });
      });
  },
  instagram      : function (req, res) {
    var data           = {
          response_type: 'code',
          client_id    : req.param('consumer_key'),
          redirect_uri : format('%s/token/instagram_hooks', sails.getBaseurl()),
          state        : (Math.floor(Math.random() * Math.pow(10, 20))).toString()
        },
        consumerSecret = req.param('consumer_secret'),
        appName        = req.param('app_name') || null;

    if (!(req.param('consumer_key') && req.param('consumer_secret'))) return res.badRequest('please give consumer_key, consumer_secret');

    var url = format('https://api.instagram.com/oauth/authorize/?%s', querystring.stringify(data));

    Token.findOrCreate(
      {
        consumerKey   : data.client_id,
        consumerSecret: consumerSecret,
        media         : 'instagram'
      },
      {
        consumerKey   : data.client_id,
        consumerSecret: consumerSecret,
        media         : 'instagram'
      },
      function (err, token) {
        if (err) return res.negotiate(err);

        token.state       = data.state;
        token.redirectUri = data.redirect_uri;
        token.appName     = token.appName || null;
        token.userId      = req.user.id;

        if (appName) token.appName = appName;

        token.save(function (err, saved) {
          if (err) return res.negotiate(err);

          res.redirect(url);
        });
      });
  },
  linkedin_hooks : function (req, res) {
    var code      = req.param('code') || null,
        state     = req.param('state') || null,
        error     = req.param('error') || null,
        errorDesc = req.param('error_description') || null;

    if (error) return res.negotiate(new Error(errorDesc));

    Token
      .findOne({
        state: state
      })
      .exec(function (err, token) {
        if (err) return res.negotiate(err);

        if (!token) return res.serverError(new Error('there\'s an error in database'));

        var data = {
          grant_type   : 'authorization_code',
          code         : code,
          redirect_uri : token.redirectUri,
          client_id    : token.consumerKey,
          client_secret: token.consumerSecret
        };

        var url = format('https://www.linkedin.com/uas/oauth2/accessToken');

        request.post({url: url, form: data}, function (error, response, body) {
          if (error) {
            console.error(error);
            return res.negotiate(error);
          }

          var parsed = null;
          try {
            parsed = JSON.parse(body);
          } catch (e) {
            return res.negotiate(e);
          }

          var accessToken = parsed.access_token,
              expiresIn   = parsed.expires_in;

          if (typeof token.accessTokens === 'undefined') token.accessTokens = [];

          var exist = _.some(token.accessTokens, {accessToken: accessToken});

          if (!exist) {
            token.accessTokens.push({
              accessToken: accessToken,
              expiresIn  : expiresIn
            });
          }

          token.save(function (err, saved) {
            if (err) return res.negotiate(err);

            saved.accessTokens = saved.accessTokens.reverse();

            res.view('token/list', {
              title : 'LinkedIn Generated Token',
              tokens: [saved]
            });
          });

        });
      });
  },
  linkedin       : function (req, res) {
    var data           = {
          response_type: 'code',
          client_id    : req.param('consumer_key'),
          redirect_uri : format('%s/token/linkedin_hooks', sails.getBaseurl()),
          state        : (Math.floor(Math.random() * Math.pow(10, 20))).toString()
        },
        consumerSecret = req.param('consumer_secret'),
        appName        = req.param('app_name') || null;

    if (!(req.param('consumer_key') && req.param('consumer_secret'))) return res.badRequest('please give consumer_key, consumer_secret');

    var url = format('https://www.linkedin.com/uas/oauth2/authorization?%s', querystring.stringify(data));

    Token.findOrCreate(
      {
        consumerKey   : data.client_id,
        consumerSecret: consumerSecret,
        media         : 'linkedin'
      },
      {
        consumerKey   : data.client_id,
        consumerSecret: consumerSecret,
        media         : 'linkedin'
      },
      function (err, token) {
        if (err) return res.negotiate(err);

        token.state       = data.state;
        token.redirectUri = data.redirect_uri;
        token.appName     = token.appName || null;
        token.userId      = req.user.id;

        if (appName) token.appName = appName;

        token.save(function (err, saved) {
          if (err) return res.negotiate(err);

          res.redirect(url);
        });
      });
  },
  twitter_hooks  : function (req, res) {
    var requestToken  = req.param('oauth_token'),
        oauthVerifier = req.param('oauth_verifier');

    Token
      .findOne({
        requestToken: requestToken
      })
      .exec(function (err, token) {
        if (err) return res.negotiate(err);

        if (!token) return res.serverError(new Error('there\'s an error in database'));

        twitter.getAccessToken(requestToken, token.requestTokenSecret, oauthVerifier, function (error, accessToken, accessTokenSecret, results) {
          if (error) {
            console.error(error);
            return res.negotiate(error);
          }

          if (typeof token.accessTokens === 'undefined') token.accessTokens = [];

          var exist = _.some(token.accessTokens, {accessToken: accessToken});

          if (!exist) {
            token.accessTokens.push({
              accessToken      : accessToken,
              accessTokenSecret: accessTokenSecret,
              userId           : results.user_id,
              screenName       : results.screen_name
            });
          }

          token.save(function (err, saved) {
            if (err) return res.negotiate(err);

            saved.accessTokens = saved.accessTokens.reverse();

            res.view('token/list', {
              title : 'Twitter Generated Token',
              tokens: [saved]
            });
          });
        });
      });
  },
  twitter        : function (req, res) {
    var data     = {
          consumerKey   : req.param('consumer_key'),
          consumerSecret: req.param('consumer_secret'),
          callback      : format('%s/token/twitter_hooks', sails.getBaseurl())
        },
        appName  = req.param('app_name') || null,
        owner    = req.param('owner') || null,
        owner_id = req.param('owner_id') || null;

    if (!(req.param('consumer_key') && req.param('consumer_secret'))) return res.badRequest('please give consumer_key, consumer_secret');

    twitter = new twitterAPI(data);
    twitter.getRequestToken(function (error, requestToken, requestTokenSecret, results) {
      if (error) {
        console.error(error);
        return res.negotiate(error);
      }

      Token.findOrCreate(
        {
          consumerKey   : data.consumerKey,
          consumerSecret: data.consumerSecret,
          media         : 'twitter'
        },
        {
          consumerKey   : data.consumerKey,
          consumerSecret: data.consumerSecret,
          media         : 'twitter'
        },
        function (err, token) {
          if (err) return res.negotiate(err);

          token.appName            = token.appName || null;
          token.requestToken       = requestToken;
          token.requestTokenSecret = requestTokenSecret;
          token.userId             = req.user.id;
          token.owner              = token.owner || null;
          token.ownerId            = token.ownerId || null;
          if (appName) token.appName = appName;
          if (owner) token.owner = owner;
          if (owner_id) token.ownerId = owner_id;

          token.save(function (err, saved) {
            if (err) return res.negotiate(err);

            res.redirect(twitter.getAuthUrl(requestToken));
          });
        });
    });
  }
};
