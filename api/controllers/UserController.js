/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  profile        : function (req, res) {
    User.findOne({id: req.user.id}).exec(function (err, user) {
      if (err) return res.negotiate(err);

      Passport
        .findOne({user: req.user.id, protocol: 'local'})
        .exec(function (err, passport) {
          if (err) return res.negotiate(err);

          var local = false;
          if (passport) local = true;

          res.view('user/profile', {
            user : user,
            local: local
          });
        });
    });
  },
  create_password: function (req, res) {
    var id       = req.user.id,
        password = req.param('password');

    if (!password) return res.badRequest('please provide \'password\' parameter');

    Passport
      .findOne({user: id, protocol: 'local'})
      .exec(function (err, rec) {
        if (err) return res.negotiate(err);
        if (rec) return res.forbidden('you already set a password');

        Passport
          .create({user: id, password: password, protocol: 'local'}, function (err, rec) {
            if (err) return res.negotiate(err);

            res.json({
              msg: 'your password was set'
            });
          });
      });
  },
  update_password: function (req, res) {
    var id          = req.user.id,
        oldPassword = req.param('old_password'),
        newPassword = req.param('new_password');

    if (!oldPassword) return res.badRequest('please provide \'old_password\' parameter');
    if (!newPassword) return res.badRequest('please provide \'new_password\' parameter');

    Passport
      .findOne({user: id, protocol: 'local'})
      .exec(function (err, rec) {
        if (err) return res.negotiate(err);
        rec.validatePassword(oldPassword, function (err, pass) {
          if (err) return res.forbidden(err);

          if (!pass) return res.badRequest('your \'old_password\' did not match');

          Passport
            .update({user: id}, {password: newPassword}, function (err, rec) {
              if (err) return res.badRequest(err);

              res.json({
                msg: 'your password was updated'
              });
            });
        })
      });
  }
};

